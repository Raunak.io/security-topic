
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import * as helmet from 'helmet';
// import * as helmet from 'fastify-helmet';

import * as rateLimit from 'express-rate-limit';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(helmet()); // prevents from web vulnerabilities 
  // sets http headers appropriately .. combination of 14 small security middlewares


// app.register(helmet) ; // used as a plugin instead of middleware

  app.enableCors(); // for cross origin resource sharing


 

  app.use(rateLimit({
    windowsMs:15*60*1000,  // restricts rate limit from a single ip address
    max:100,   // prevents from brute force attacks
  }))

 
  await app.listen(8000);
}
bootstrap();
